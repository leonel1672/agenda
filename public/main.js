var socket = io.connect('http://localhost:8080', {'forceNew' : true})

socket.on('messages', function(data){
    console.log(data)
    render(data);
})

function render(data){
    var html = data.map(function(data, index){
        return(`<tr>
                    <td>${data.name}</td>
                    <td>${data.lastName}</td>
                    <td>${data.telephone}</td>
                    <td>${data.email}</td>
                </tr>`)
    }).join(" ")

document.getElementById('messages').innerHTML = html;

$("input[type=text], input[type=number], input[type=email]").val("");
}

function addMessage(e){
    var payload = {
        name       :  document.getElementById('name').value,
        lastName   :  document.getElementById('lastName').value,
        telephone  :  document.getElementById('telephone').value,
        email      :  document.getElementById('email').value
    };

    socket.emit('new-messages', payload);
    return false;
}
